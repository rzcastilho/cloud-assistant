defmodule ExKube.Info do

    @help_params_1 "Please, inform a supported resource kind."
    @help_params_2 "Please, inform a supported resource kind and a resource name that exists in the cluster."

    @not_implemented "Sorry, resource kind not implemented yet."

    def get(:deployment, resource) do
        case do_get(:deployment, resource) do
            {:success, %{"status" => %{"availableReplicas" => _, "unavailableReplicas" => not_running, "replicas" => desired}}} when not_running == desired ->
                {:success, "There is a problem with deployment #{resource}, neither of #{desired} desired replicas is running."}
            {:success, %{"status" => %{"availableReplicas" => _, "unavailableReplicas" => not_running, "replicas" => desired}}} when not_running == 1 or not_running == 0 ->
                {:success, "This could be a problem, there is #{not_running} unavailable replica from a total of #{desired}"}
            {:success, %{"status" => %{"availableReplicas" => _, "unavailableReplicas" => not_running, "replicas" => desired}}} ->
                {:success, "This could be a problem, there are #{not_running} unavailable replicas from a total of #{desired}"}
            {:success, %{"status" => %{"availableReplicas" => _, "replicas" => desired}}} when desired == 1 or desired == 0  ->
                {:success, "The deployment #{resource} is OK. There is #{desired} available replica from a total of one."}
            {:success, %{"status" => %{"availableReplicas" => running, "replicas" => desired}}} ->
                {:success, "The deployment #{resource} is OK. There are #{running} available replicas from a total of #{desired}."}
            {:error, message} ->
                {:error, message}
        end
    end

    def get(:statefulset, resource) do
        case do_get(:statefulset, resource) do
            {:success, %{"spec" => %{"replicas" => spec_replicas}, "status" => %{"currentReplicas" => _, "readyReplicas" => _, "replicas" => replicas}}} when spec_replicas > replicas ->
                {:success, "The stateful set #{resource} is scaling up. Wait some seconds to get more information."}
            {:success, %{"spec" => %{"replicas" => spec_replicas}, "status" => %{"currentReplicas" => _, "readyReplicas" => _, "replicas" => replicas}}} when spec_replicas < replicas ->
                {:success, "The stateful set #{resource} is scaling down. Wait some seconds to get more information."}
            {:success, %{"spec" => %{"replicas" => spec_replicas}, "status" => %{"currentReplicas" => current_replicas, "readyReplicas" => ready_replicas, "replicas" => replicas}}} when spec_replicas == current_replicas and spec_replicas == ready_replicas and spec_replicas == replicas and (spec_replicas == 1 or spec_replicas == 0) ->
                {:success, "The stateful set #{resource} is OK. There is #{spec_replicas} ready replica from a total of one."}
            {:success, %{"spec" => %{"replicas" => spec_replicas}, "status" => %{"currentReplicas" => current_replicas, "readyReplicas" => ready_replicas, "replicas" => replicas}}} when spec_replicas == current_replicas and spec_replicas == ready_replicas and spec_replicas == replicas ->
                {:success, "The stateful set #{resource} is OK. There are #{ready_replicas} ready replicas from a total of #{spec_replicas}."}
            {:success, %{"spec" => %{"replicas" => spec_replicas}, "status" => %{"currentReplicas" => _, "readyReplicas" => ready_replicas, "replicas" => _}}} when spec_replicas != ready_replicas and (ready_replicas == 1 or ready_replicas == 0) ->
                {:success, "This could be a problem, there are #{ready_replicas} ready replica from a total of #{spec_replicas}"}
            {:success, %{"spec" => %{"replicas" => spec_replicas}, "status" => %{"currentReplicas" => _, "readyReplicas" => ready_replicas, "replicas" => _}}} when spec_replicas != ready_replicas ->
                {:success, "This could be a problem, there are #{ready_replicas} ready replicas from a total of #{spec_replicas}"}
            {:error, message} ->
                {:error, message}
        end
    end

    def get(:replicaset, _resource) do
        {:error, @not_implemented}
    end

    def get(:replicationcontroller, _resource) do
        {:error, @not_implemented}
    end

    def get(:certificatesigningrequest, _resource) do
        {:error, @not_implemented}
    end

    def get(:clusterrolebinding, _resource) do
        {:error, @not_implemented}
    end

    def get(:clusterrole, _resource) do
        {:error, @not_implemented}
    end

    def get(:cluster, _resource) do
        {:error, @not_implemented}
    end

    def get(:componentstatus, _resource) do
        {:error, @not_implemented}
    end

    def get(:configmap, _resource) do
        {:error, @not_implemented}
    end

    def get(:controllerrevision, _resource) do
        {:error, @not_implemented}
    end

    def get(:cronjob, _resource) do
        {:error, @not_implemented}
    end

    def get(:custom_resourcedefinition, _resource) do
        {:error, @not_implemented}
    end

    def get(:daemonset, _resource) do
        {:error, @not_implemented}
    end

    def get(:endpoint, _resource) do
        {:error, @not_implemented}
    end

    def get(:event, _resource) do
        {:error, @not_implemented}
    end

    def get(:horizontalpodautoscaler, _resource) do
        {:error, @not_implemented}
    end

    def get(:ingress, _resource) do
        {:error, @not_implemented}
    end

    def get(:job, _resource) do
        {:error, @not_implemented}
    end

    def get(:limitrange, _resource) do
        {:error, @not_implemented}
    end

    def get(:namespace, _resource) do
        {:error, @not_implemented}
    end

    def get(:networkpolicie, _resource) do
        {:error, @not_implemented}
    end

    def get(:node, _resource) do
        {:error, @not_implemented}
    end

    def get(:persistentvolumeclaim, _resource) do
        {:error, @not_implemented}
    end

    def get(:persistentvolume, _resource) do
        {:error, @not_implemented}
    end

    def get(:poddisruptionbudget, _resource) do
        {:error, @not_implemented}
    end

    def get(:podpreset, _resource) do
        {:error, @not_implemented}
    end

    def get(:pod, _resource) do
        {:error, @not_implemented}
    end

    def get(:podsecuritypolicy, _resource) do
        {:error, @not_implemented}
    end

    def get(:podtemplate, _resource) do
        {:error, @not_implemented}
    end

    def get(:_resourcequota, _resource) do
        {:error, @not_implemented}
    end

    def get(:rolebinding, _resource) do
        {:error, @not_implemented}
    end

    def get(:role, _resource) do
        {:error, @not_implemented}
    end

    def get(:secret, _resource) do
        {:error, @not_implemented}
    end

    def get(:serviceaccount, _resource) do
        {:error, @not_implemented}
    end

    def get(:service, _resource) do
        {:error, @not_implemented}
    end

    def get(:storageclass, _resource) do
        {:error, @not_implemented}
    end

    def get(_, _) do
        {:error, @help_params_2}
    end

    def get(:overview) do
        {:success, pods} = get(:pod)
        {
            :success,
            [:node, :service, :ingress, :deployment, :statefulset]
                |> Enum.map(fn resource_kind -> Task.async(fn -> get(resource_kind) end) end)
                |> Enum.map(fn task -> Task.await(task, 10000) end)
                |> Enum.filter(fn {status, _} -> status == :success end)
                |> Enum.map(fn {:success, message} -> message end)
                |> Enum.intersperse(", ")
                |> Enum.concat([" and ", pods])
                |> Enum.join
        }
    end

    def get(:node) do
        case do_get(:node) do
            {:success, json} ->
                case json["items"] |> Enum.filter(fn item -> item["kind"] == "Node" end) |> Enum.count do
                    0 ->
                        {:success, "no nodes"}
                    1 ->
                        {:success, "1 node"}
                    x ->
                        {:success, "#{x} nodes"}
                end
            {:error, message} ->
                {:error, message}
        end
    end

    def get(:service) do
        case do_get(:service) do
            {:success, json} ->
                case json["items"] |> Enum.filter(fn item -> item["kind"] == "Service" end) |> Enum.count do
                    0 ->
                        {:success, "no services"}
                    1 ->
                        {:success, "1 service"}
                    x ->
                        {:success, "#{x} services"}
                end
            {:error, message} ->
                {:error, message}
        end
    end

    def get(:ingress) do
        case do_get(:ingress) do
            {:success, json} ->
                case json["items"] |> Enum.filter(fn item -> item["kind"] == "Ingress" end) |> Enum.count do
                    0 ->
                        {:success, "no ingresses"}
                    1 ->
                        {:success, "1 ingress"}
                    x ->
                        {:success, "#{x} ingresses"}
                end
            {:error, message} ->
                {:error, message}
        end
    end

    def get(:deployment) do
        case do_get(:deployment) do
            {:success, json} ->
                case json["items"] |> Enum.filter(fn item -> item["kind"] == "Deployment" end) |> Enum.count do
                    0 ->
                        {:success, "no deployments"}
                    1 ->
                        {:success, "1 deployment"}
                    x ->
                        {:success, "#{x} deployments"}
                end
            {:error, message} ->
                {:error, message}
        end
    end

    def get(:statefulset) do
        case do_get(:statefulset) do
            {:success, json} ->
                case json["items"] |> Enum.filter(fn item -> item["kind"] == "StatefulSet" end) |> Enum.count do
                    0 ->
                        {:success, "no statefulsets"}
                    1 ->
                        {:success, "1 statefulset"}
                    x ->
                        {:success, "#{x} statefulsets"}
                end
            {:error, message} ->
                {:error, message}
        end
    end

    def get(:pod) do
        case do_get(:pod) do
            {:success, json} ->
                case json["items"] |> Enum.filter(fn item -> item["kind"] == "Pod" end) |> Enum.count do
                    0 ->
                        {:success, "no pods"}
                    1 ->
                        {:success, "1 pod"}
                    x ->
                        {:success, "#{x} pods"}
                end
            {:error, message} ->
                {:error, message}
        end
    end

    def get(_) do
        {:error, @help_params_1}
    end

    defp do_get(resource_type, resource) do
        case System.cmd("kubectl", ["get", resource_type,  resource, "-o", "json"], [stderr_to_stdout: true]) do
            {message, 0} ->
                {:success, Poison.decode!(message)}
            {message, _} ->
                {:error, String.trim("#{@help_params_2}. Detailed Message: #{message}")}
        end
    end

    defp do_get(resource_type) do
        case System.cmd("kubectl", ["get", resource_type, "-o", "json"], [stderr_to_stdout: true]) do
            {message, 0} ->
                {:success, Poison.decode!(message)}
            {message, _} ->
                {:error, String.trim("#{@help_params_2}. Detailed Message: #{message}")}
        end
    end

end
