defmodule ExKube.Action do

    @help "Please, inform a supported resource kind, a resource name that exists in the cluster and a desired number of replicas."
    
    def scale(:deployment, resource, replicas) when is_integer(replicas) do
        do_scale(:deployment, resource, replicas)
    end

    def scale(:replicaset, resource, replicas) when is_integer(replicas) do
        do_scale(:replicaset, resource, replicas)
    end

    def scale(:replicationcontroller, resource, replicas) when is_integer(replicas) do
        do_scale(:replicationcontroller, resource, replicas)
    end

    def scale(:statefulset, resource, replicas) when is_integer(replicas) do
        do_scale(:statefulset, resource, replicas)
    end

    def scale(_, _, _) do
        {:error, @help}
    end

    defp do_scale(resource_type, resource, replicas) do
        case System.cmd("kubectl", ["scale", resource_type,  resource, "--replicas=#{replicas}"], [stderr_to_stdout: true]) do
            {message, 0} ->
                {:success, String.trim("#{message} to #{replicas} replicas")}
            {message, _} ->
                {:error, String.trim("#{@help}. Detailed Message: #{message}")}
        end
    end

end