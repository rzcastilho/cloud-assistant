defmodule ExKube do
  @moduledoc """
  Documentation for ExKube.
  """

  @doc """
  Hello world.

  ## Examples

      iex> ExKube.hello
      :world

  """
  def hello do
    :world
  end
end
