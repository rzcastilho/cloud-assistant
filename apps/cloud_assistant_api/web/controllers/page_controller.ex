defmodule CloudAssistantApi.PageController do
  use CloudAssistantApi.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
