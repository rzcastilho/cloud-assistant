defmodule CloudAssistantApi.ApiController do
    use CloudAssistantApi.Web, :controller

    def execute(%Plug.Conn{method: method, body_params: %{"request" => request, "session" => session} = body, req_headers: headers} = conn, _params) do
        IO.inspect method
        IO.inspect headers
        IO.inspect session
        IO.inspect request
        output = %{version: "1.0", sessionAttributes: session["attributes"], response: %{}}
        case do_intent(request) do
            {:success, message} ->
                output = put_in(output.response, %{shouldEndSession: false, outputSpeech: %{type: "PlainText", text: message}})
                conn
                |> put_resp_content_type("application/json")
                |> send_resp(200, Poison.encode!(output))
            {:continue} ->
                %{"intent" => intent} = request
                output = put_in(output.response, %{shouldEndSession: false, directives: [%{type: "Dialog.Delegate", updatedIntent: intent}]})
                conn
                |> put_resp_content_type("application/json")
                |> send_resp(200, Poison.encode!(output))
            {:not_implemented, intent} ->
                output = put_in(output.response, %{outputSpeech: %{type: "PlainText", text: "Sorry! Intent '#{intent}' is not implemented yet."}})
                conn
                |> put_resp_content_type("application/json")
                |> send_resp(200, Poison.encode!(output))
            {:error, message} ->
                output = put_in(output.response, %{outputSpeech: %{type: "PlainText", text: message}})
                conn
                |> put_resp_content_type("application/json")
                |> send_resp(200, Poison.encode!(output))
        end

    end

    defp do_intent(%{"type" => "SessionEndedRequest"}) do
        {:error, "Ending session due to lack of interactivity."}
    end

    defp do_intent(%{"intent" => %{"name" => "scaleResource", "slots" => slots}} = request) do
        IO.inspect request
        params = case slots do
            %{"resource_kind" => %{"name" => "resource_kind", "value" => resource_kind_value, "resolutions" => resource_kind_resolution}, "resource_name" => %{"name" => "resource_name", "value" => resource_name_value, "resolutions" => resource_name_resolution},"scale_number" => %{"name" => "scale_number", "value" => scale_number_value}} ->
                {String.to_atom(resolve_match(resource_kind_resolution, resource_kind_value)), resolve_match(resource_name_resolution, resource_name_value), scale_number_value}
            %{"resource_kind" => %{"name" => "resource_kind", "value" => resource_kind_value, "resolutions" => resource_kind_resolution}, "resource_name" => %{"name" => "resource_name", "value" => resource_name_value},"scale_number" => %{"name" => "scale_number", "value" => scale_number_value}} ->
                {String.to_atom(resolve_match(resource_kind_resolution, resource_kind_value)), resource_name_value, scale_number_value}
            %{"resource_kind" => %{"name" => "resource_kind", "value" => resource_kind_value}, "resource_name" => %{"name" => "resource_name", "value" => resource_name_value, "resolutions" => resource_name_resolution},"scale_number" => %{"name" => "scale_number", "value" => scale_number_value}} ->
                {String.to_atom(resource_kind_value), resolve_match(resource_name_resolution, resource_name_value), scale_number_value}
            %{"resource_kind" => %{"name" => "resource_kind", "value" => resource_kind_value}, "resource_name" => %{"name" => "resource_name", "value" => resource_name_value},"scale_number" => %{"name" => "scale_number", "value" => scale_number_value}} ->
                {String.to_atom(resource_kind_value), resource_name_value, scale_number_value}
            _ ->
                {:continue}
        end
        IO.inspect params
        case params do
            {resource_kind, resource_name, scale_number} ->
                case String.to_integer(scale_number) do
                    replicas when replicas > 10 ->
                        {:error, "Take it easy! You can't scale a resource more than ten replicas!"}
                    replicas when replicas < 0 ->
                        {:error, "Don't be stupid! A resource cannot be scaled to less than zero!"}
                    replicas ->
                        ExKube.Action.scale(resource_kind, resource_name, replicas)
                end
            _ ->
                {:continue}
        end
    end

    defp do_intent(%{"intent" => %{"name" => "resourceInfo", "slots" => slots}} = request) do
        IO.inspect request
        case slots do
            %{"resource_kind" => %{"name" => "resource_kind", "value" => resource_kind, "resolutions" => resource_kind_resolution}, "resource_name" => %{"name" => "resource_name", "value" => resource_name, "resolutions" => resource_name_resolution}} ->
                ExKube.Info.get(String.to_atom(resolve_match(resource_kind_resolution, resource_kind)), resolve_match(resource_name_resolution, resource_name))
            %{"resource_kind" => %{"name" => "resource_kind", "value" => resource_kind, "resolutions" => resource_kind_resolution}, "resource_name" => %{"name" => "resource_name", "value" => resource_name}} ->
                ExKube.Info.get(String.to_atom(resolve_match(resource_kind_resolution, resource_kind)), resource_name)
            %{"resource_kind" => %{"name" => "resource_kind", "value" => resource_kind}, "resource_name" => %{"name" => "resource_name", "value" => resource_name, "resolutions" => resource_name_resolution}} ->
                ExKube.Info.get(String.to_atom(resource_kind), resolve_match(resource_name_resolution, resource_name))
            %{"resource_kind" => %{"name" => "resource_kind", "value" => resource_kind}, "resource_name" => %{"name" => "resource_name", "value" => resource_name}} ->
                ExKube.Info.get(String.to_atom(resource_kind), resource_name)
            _ ->
                {:continue}
        end
    end

    defp do_intent(%{"intent" => %{"name" => "clusterOverview", "slots" => %{"cluster_name" => %{"name" => "cluster_name", "value" => cluster_name}}}} = request) do
        IO.inspect request
        case ExKube.Info.get(:overview) do
            {:success, message} ->
                {:success, "Here we go! In cluster #{cluster_name}, there are #{message}."}
            {:error, message} ->
                {:error, message}
        end
    end

    defp do_intent(%{"intent" => %{"name" => intent}} = request) do
        IO.inspect request
        {:not_implemented, intent}
    end

    defp resolve_match(resolutions, default) do
        authority = hd(resolutions["resolutionsPerAuthority"])
        case authority do
            %{"status" => %{"code" => "ER_SUCCESS_MATCH"}, "values" => values} ->
                hd(values)["value"]["name"]
            _ ->
                default
        end
    end

end
