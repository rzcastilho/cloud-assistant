defmodule CloudAssistantApi.Router do
  use CloudAssistantApi.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/ui", CloudAssistantApi do
    pipe_through :browser # Use the default browser stack
    get "/", PageController, :index
  end

  scope "/", CloudAssistantApi do
    pipe_through :api
    match :*, "/*path", ApiController, :execute
  end

end
