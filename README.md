[![](https://images.microbadger.com/badges/image/rodrigozc/cloud-assistant.svg)](https://microbadger.com/images/rodrigozc/cloud-assistant "Get your own image badge on microbadger.com") [![](https://images.microbadger.com/badges/version/rodrigozc/cloud-assistant.svg)](https://microbadger.com/images/rodrigozc/cloud-assistant "Get your own version badge on microbadger.com")

# Cloud Assistant

__Phoenix__ application that implements __Cloud Assistant Alexa Skill__ `beta` to interact with __Kubernetes Clusters__.

To use this assistant, you have to deploy it in a kubernetes cluster and expose as a service. The cluster must be added to __DynamoDB__ __ClusterPermissions__ table associated to the user_id that will be used to trigger the skill.

In the following actions, there are examples to deploy, expose and authorize __Cloud Assistant__ in __Kubernetes Cluster__.

### Deploy

To deploy __Cloud Assistant__, apply the file below and change as needed.

`$ kubectl apply -f kubernetes/cloud-assistant.deployment.yaml`

```yaml
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: cloud-assistant
  labels:
    run: cloud-assistant
spec:
  replicas: 1
  selector:
    matchLabels:
      run: cloud-assistant
  strategy:
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 1
    type: RollingUpdate
  template:
    metadata:
      labels:
        run: cloud-assistant
    spec:
      containers:
      - image: rodrigozc/cloud-assistant:latest
        imagePullPolicy: Always
        name: cloud-assistant
        ports:
        - containerPort: 4000
          name: http
          protocol: TCP
        readinessProbe:
          httpGet:
            path: /ui
            port: 4000
          initialDelaySeconds: 30
          timeoutSeconds: 5
          periodSeconds: 30
          failureThreshold: 3
      restartPolicy: Always
      terminationGracePeriodSeconds: 30
```

### Service

In this example, the assistant is exposed with a fixed __NodePort__, this makes the assistant accessible from any machine from cluster.

`$ kubectl apply -f kubernetes/cloud-assistant.service.yaml`

```yaml
kind: Service
apiVersion: v1
metadata:
  name: cloud-assistant
  labels:
    run: cloud-assistant
spec:
  type: NodePort
  selector:
    run: cloud-assistant
  ports:
  - name: http
    port: 4000
    targetPort: 4000
    nodePort: 31099

```

### Authorization

The __Cloud Assistant__ must have access to __API Groups__ and __Resources__ to be able to make operations on cluster.

`$ kubectl apply -f kubernetes/cloud-assistant.auth.yaml`

```yaml
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1alpha1
metadata:
  name: cloud-assistant-role
rules:
  - apiGroups: ["", "apps", "extensions"]
    resources: ["pods", "nodes", "ingresses", "services", "deployments", "statefulsets"]
    verbs: ["get", "list", "watch", "create", "delete", "update"]
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: cloud-assistant-rbac
subjects:
  - kind: ServiceAccount
    namespace: default
    name: default
roleRef:
  kind: ClusterRole
  name: cloud-assistant-role
  apiGroup: rbac.authorization.k8s.io
```
